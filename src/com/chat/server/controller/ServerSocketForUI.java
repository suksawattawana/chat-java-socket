package com.chat.server.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;

import com.chat.frame.ChatApplication;
import com.chat.server.cores.model.Message;
import com.chat.server.cores.serives.ClientHandler;
import com.chat.util.ColorUtils;
import com.chat.util.FontUtils;

public class ServerSocketForUI implements Runnable {

	public HashMap<Integer, ClientHandler> clientHandlerList = new HashMap<Integer, ClientHandler>();
	private ServerSocket serverSocket = null;
	public Thread thread = null;
	private ChatApplication ui;

	public ServerSocketForUI() {
	}

	public ServerSocketForUI(ChatApplication ui) {
		super();
		this.ui = ui;
	}

	public void startServerSocket(int port) {
		try {
			serverSocket = new ServerSocket(port);
			ui.btnConnect.setEnabled(false);
			ui.statusServer = true;
			ui.txtSatusServer.setText("On");
			ui.txtSatusServer.setForeground(ColorUtils.green());
			ui.areaMsg.insertString(ui.areaMsg.getLength(), "Server is started... \n IP : " + InetAddress.getLocalHost() + "\n Port : "
					+ serverSocket.getLocalPort(), FontUtils.blueStyle());

			clientReciept();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void clientReciept() throws Exception {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				ui.areaMsg.insertString(ui.areaMsg.getLength(), "\nWaiting for client connect...", FontUtils.blueStyle());
				addClientHandler(serverSocket.accept());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void addClientHandler(Socket socket) {
		try {
			Integer id = socket.getPort();
			clientHandlerList.put(id, new ClientHandler(socket, this));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public synchronized void handle(Integer id, Message msg) {
		Iterator<Integer> allClientMap = clientHandlerList.keySet().iterator();
		while (allClientMap.hasNext()) {
			Integer key = allClientMap.next(); // Key
			ClientHandler serverThread = clientHandlerList.get(key);
			serverThread.send(msg);
		}
	}

	public synchronized void remove(Integer id) {
		try {
			ClientHandler serverThread = clientHandlerList.get(id);
			serverThread.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			clientHandlerList.remove(id);
		}
	}

}
package com.chat.server.controller;

import com.chat.server.cores.serives.ServerManager;

public class ApplicationServer {

	public static void main(String[] args) {
		
		ServerManager manger = new ServerManager();
		try {
			System.out.println("Will starting server in port " + 13000);
			manger.startAppServer(13000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.chat.server.cores.serives;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;

import com.chat.server.cores.model.Message;

public class ServerService {

	private HashMap<Integer,ServerThread> clientHandlerList = new HashMap<Integer,ServerThread>();
	private ServerSocket serverSocket = null;
	

	String startServerSocket(int port) throws IOException {
		String result = "";

		try {
			serverSocket = new ServerSocket(port);
			result = "Server startet. IP : " + InetAddress.getLocalHost() + ", Port : " + serverSocket.getLocalPort();
		} catch (IOException e) {
			throw e;
		}

		return result;
	}
	
	void clientReciept() throws Exception {
		while (true) {
			try {
				System.out.println("Waiting for client connect...");
				addClientHandler(serverSocket.accept());
			} catch (IOException e) {
				throw e;
			}
		}
	}
	
	private void addClientHandler(Socket socket) {
		try {
			Integer id = socket.getPort();
			clientHandlerList.put(id, new ServerThread(socket, this));
			System.out.println("Success for regis client : " + socket);
			System.out.println("client id: " + socket.getPort());
			System.out.println("number of client connect: " + clientHandlerList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void handle(Integer id, Message msg) {
		System.out.println("id: " + id);
		Iterator<Integer> allClientMap = clientHandlerList.keySet().iterator();
		while (allClientMap.hasNext()) {
			Integer key = allClientMap.next();  // Key
			ServerThread serverThread = clientHandlerList.get(key);
			if(id != (int)key) {
				serverThread.send(msg);
				System.out.println("send to " + key);
			}
		}
	}
	
	public synchronized void remove(Integer id) {
		try {
			ServerThread serverThread = clientHandlerList.get(id);
			serverThread.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			clientHandlerList.remove(id);
			System.out.println("number of client connect: " + clientHandlerList.size());
		}
	}
}

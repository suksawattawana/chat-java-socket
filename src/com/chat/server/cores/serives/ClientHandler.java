package com.chat.server.cores.serives;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.chat.server.controller.ServerSocketForUI;
import com.chat.server.cores.model.Message;

public class ClientHandler extends Thread {

	public Integer id = -1;
	private Socket socket;
	private ServerSocketForUI serverService;
	private ObjectInputStream streamIn = null;
	private ObjectOutputStream streamOut = null;

	public ClientHandler(Socket socket, ServerSocketForUI serverService) throws Exception {
		super();
		this.socket = socket;
		this.serverService = serverService;
		id = socket.getPort();

		try {
			openStream();
			start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	void openStream() throws Exception {
		try {
			streamOut = new ObjectOutputStream(socket.getOutputStream());
			streamOut.flush();
			streamIn = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			throw e;
		}
	}

	public void send(Message msg) {
		try {
			streamOut.writeObject(msg);
			streamOut.flush();
		} catch (IOException ex) {
			System.out.println("Exception [SocketClient : send(...)]");
		}
	}

	public void close() throws IOException {
		if (socket != null)
			socket.close();
		if (streamIn != null)
			streamIn.close();
		if (streamOut != null)
			streamOut.close();
	}

	public void run() {
		while (true) {
			try {
				Message msg = (Message) streamIn.readObject();
				serverService.handle(id, msg);
			} catch (Exception ioe) {
				System.out.println("ERROR reading: " + ioe.getMessage());
				serverService.remove(id);
			}
		}
	}

}

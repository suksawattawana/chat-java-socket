package com.chat.server.cores.serives;

import java.io.IOException;

public class ServerManager {
	
	private ServerService service;
	
	public ServerManager() {}

	// cmd 
	public String startAppServer(int port) throws Exception{
		String result = "";
		
		if (port > 0) {
			service = new ServerService();
			try {
				result = service.startServerSocket(port);
				System.out.println(result);
				
				service.clientReciept();
			} catch (IOException e) {
				throw e;
			}
		}
		else {
			result = "Invalid port";
		}
		
		return result;
	}
	
}

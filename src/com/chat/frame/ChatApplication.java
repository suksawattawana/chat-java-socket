package com.chat.frame;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyledDocument;

import com.chat.client.controller.SocketClientForUI;
import com.chat.client.cores.model.MsgClient;
import com.chat.server.controller.ServerSocketForUI;
import com.chat.util.ColorUtils;
import com.chat.util.ComponentUtils;
import com.chat.util.FontUtils;
import com.chat.util.RowHandler;

import net.miginfocom.swing.MigLayout;

public class ChatApplication extends JFrame {

	private static final long serialVersionUID = 1L;
	private int x_window, y_window, size_width = 800, size_height = 500;
	private ResourceBundle bundle;
	private SocketClientForUI socketClien = new SocketClientForUI(this);
	private ServerSocketForUI serverSocket = new ServerSocketForUI(this);
	public boolean statusServer = false; // true-on, false-off
	private char path; // s-server, c-client
	RowHandler rowContentPane = new RowHandler();
	RowHandler rowPanelStatus = new RowHandler();

	private JPanel contentPane;
	private JPanel panelStatus;
	private JLabel txtPort;
	private JLabel txtIP;
	private JTextField fieldIP;
	private JTextField fieldPort;
	public JTextField fieldMsg;
	public JButton btnConnect;
	public JTextPane textPaneMsg;
	private JScrollPane jScrollArea;

	public JButton btnSend;
	private JLabel txtServer;
	public JLabel txtSatusServer;
	public StyledDocument areaMsg;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ChatApplication().setVisible(true);
			}
		});
	}

	public ChatApplication() {
		bundle = ResourceBundle.getBundle("resources/properties/messages", Locale.UK);
		findScreenSizeWindowForProtionStart();
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBounds(x_window, y_window, size_width, size_height);
		setIconImage(checkAndGetImage(bundle.getString("imgTitle")));
		setTitle(bundle.getString("title"));
		customComponent();
		actionComponent();
		manageLayout();
		setContentPane(contentPane);
	}

	/*
	 * manage position window screen when start application, position form center
	 */
	private void findScreenSizeWindowForProtionStart() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		x_window = (int) (screenSize.getWidth() / 2) - (size_width / 2);
		y_window = (int) (screenSize.getHeight() / 2) - (size_height / 2);
	}

	private void customComponent() {
		contentPane = new JPanel();
		panelStatus = new JPanel();

		// ��ǹ�ͧ����Ѻ���
		txtIP = FontUtils.font16B(bundle.getString("ip"));
		txtPort = FontUtils.font16B(bundle.getString("port"));
		fieldIP = ComponentUtils.fieldInput(30);
		fieldPort = ComponentUtils.fieldInput(30);
		btnConnect = ComponentUtils.btnButton(bundle.getString("connect"), 10, false);

		// �ʴ�ʶҹ�
		txtServer = FontUtils.font16(bundle.getString("server"));
		txtSatusServer = FontUtils.font16(bundle.getString("off"), ColorUtils.red());

		// �ʴ� message
		textPaneMsg = ComponentUtils.textPane(false);
		areaMsg = textPaneMsg.getStyledDocument();
		jScrollArea = ComponentUtils.jScrollPanel(textPaneMsg);

		// ��ǹ�ͧ�觢�ͤ���
		fieldMsg = ComponentUtils.fieldInput(false);
		btnSend = ComponentUtils.btnButton(bundle.getString("send"), 10, false);
	}

	private void manageLayout() {
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new MigLayout("", "[][grow][grow][grow][]", "[][grow][grow][grow][][][]"));
		contentPane.setBackground(ColorUtils.mainColor());

		// ��ǹ�ͧ����Ѻ���
		contentPane.add(txtIP, rowContentPane.manageRow(0));
		contentPane.add(fieldIP, rowContentPane.manageRow(1, "growx"));
		contentPane.add(txtPort, rowContentPane.manageRow(2));
		contentPane.add(fieldPort, rowContentPane.manageRow(3, "growx"));
		contentPane.add(btnConnect, rowContentPane.manageRow(4));

		// �ʴ�ʶҹ�
		panelStatus.setLayout(new MigLayout("", "[][]", "[]"));
		contentPane.add(panelStatus, rowContentPane.manageRow(0, 5, 1, "grow", true));
		panelStatus.setBackground(ColorUtils.mainColor());
		panelStatus.add(txtServer, rowPanelStatus.manageRow(0));
		panelStatus.add(txtSatusServer, rowPanelStatus.manageRow(1));

		// �ʴ� message
		contentPane.add(jScrollArea, rowContentPane.manageRow(0, 5, 1, "grow", true));

		// ��ǹ�ͧ�觢�ͤ���
		contentPane.add(fieldMsg, rowContentPane.manageRow(0, 4, 1, "growx", true));
		contentPane.add(btnSend, rowContentPane.manageRow(4, "growx", false));

		// mock data
		fieldPort.setText("8080");
	}

	/*
	 * for manage component action
	 */
	private void actionComponent() {

		// start socket server or client
		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startSocket(e);
			}
		});

		// Send massage
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sendMassage(e);
			}
		});

		fieldIP.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				checkUserInput(e);
			}
		});

		fieldPort.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				checkUserInput(e);
			}
		});
	}

	// start server or client
	private void startSocket(ActionEvent evt) {
		int port = checkAndParsePort(fieldPort.getText());

		if (path == 's' && !statusServer) {
			serverSocket.startServerSocket(port);
			fieldIP.setText("localhost"); // mock data
		} else if (path == 'c') {
			socketClien.connectServer(fieldIP.getText(), port);
		}
	}

	// check path to server or client
	private void checkUserInput(FocusEvent evt) {

		String port = fieldPort.getText();
		String ip = fieldIP.getText();

		// client
		if ((port != null && !port.isEmpty()) && (ip != null && !ip.isEmpty())) {
			path = 'c';
			btnConnect.setEnabled(true);
		}
		// server
		else if ((port != null && !port.isEmpty()) && (ip == null || ip.isEmpty())) {
			path = 's';
			btnConnect.setEnabled(true);
		} else {
			btnConnect.setEnabled(false);
		}
	}

	// send massage
	private void sendMassage(ActionEvent evt) {
		if (fieldMsg.getText() != null && !fieldMsg.getText().isEmpty()) {
			socketClien.send(new MsgClient("message", "SARM", fieldMsg.getText(), "All"));
			fieldMsg.setText("");
		}
	}

	// check port
	private int checkAndParsePort(String port) {
		int result = Integer.parseInt(port);
		return result;
	}

	/*
	 * check image path and get image
	 */
	private Image checkAndGetImage(String path) {
		try {
			if (path != null && !path.isEmpty()) {
				URL classPath = getClass().getResource(path);
				return Toolkit.getDefaultToolkit().getImage(classPath);
			}
		} catch (Exception e) {
		}
		return null;
	}

}

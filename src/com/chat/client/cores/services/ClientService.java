package com.chat.client.cores.services;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;

import com.chat.client.cores.model.MsgClient;
import com.chat.enums.MassageType;

public class ClientService implements Runnable {
	
	
	Socket socket = null;
	public ObjectInputStream In;
	public ObjectOutputStream Out;

	String connectServer(String arr, int port) throws UnknownHostException, IOException {
		String result = "";
		try {
			socket = new Socket(arr,port);
			
			Out = new ObjectOutputStream(socket.getOutputStream());
			Out.flush();
			In = new ObjectInputStream(socket.getInputStream());
			
			result = "Connect server succesfull";
		} catch (Exception e) {
			result = "Cannot Connect Server!";
		}
		
		return result;
	}
	
	void send(MsgClient msg) {
		try {
			
			
			Out.writeObject(msg);
			Out.flush();

			if (msg.getType().equals(MassageType.MASSAGE.getValue())) {
				String msgTime = (new Date()).toString();
				try {
					System.out.println(msgTime + " Me: " + msg.getContent());
				} catch (Exception ex) {
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				MsgClient msg = (MsgClient) In.readObject();
				
				System.out.println("[" + msg.getSender() + " > Me] : " + msg.getContent());
			} catch (Exception e) {
			}
		}
	}
}

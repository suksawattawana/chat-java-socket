package com.chat.client.cores.services;

import java.io.IOException;
import java.net.UnknownHostException;

import com.chat.client.cores.model.MsgClient;

public class ClientManager {

	private ClientService clientService = new ClientService();
	private Thread clientThread = new Thread(clientService);

	public String startAppClient(String arr, int port) throws UnknownHostException, IOException {
		String result = "";

		if (arr.isEmpty() || arr == null || port < 0) {
			result = "Address is Empty or Invalid port";
			return result;
		}

		result = clientService.connectServer(arr, port);
		clientThread.start();

		return result;
	}

	@SuppressWarnings("deprecation")
	public void send(MsgClient msg) {
		try {
			if (!msg.getContent().equals("exit")) {
				clientService.send(msg);
			} else {
				clientThread.stop();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.chat.client.cores.model;

import com.chat.server.cores.model.Message;

public class MsgClient extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MsgClient(String type, String sender, String content, String recipient) {
		super();
		this.type = type;
		this.sender = sender;
		this.content = content;
		this.recipient = recipient;
	}
	
}

package com.chat.client.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.chat.client.cores.model.MsgClient;
import com.chat.frame.ChatApplication;
import com.chat.util.FontUtils;

public class SocketClientForUI implements Runnable {

	Thread thread = null;
	Socket socket = null;
	public ObjectInputStream In;
	public ObjectOutputStream Out;
	private ChatApplication ui;

	public SocketClientForUI() {
	}

	public SocketClientForUI(ChatApplication ui) {
		super();
		this.ui = ui;
	}

	public void connectServer(String arr, int port) {
		try {
			socket = new Socket(arr, port);
			Out = new ObjectOutputStream(socket.getOutputStream());
			Out.flush();
			In = new ObjectInputStream(socket.getInputStream());
			start();

			ui.fieldMsg.setEnabled(true);
			ui.btnSend.setEnabled(true);
			ui.areaMsg.insertString(ui.areaMsg.getLength(), "\nSuccess connect server", FontUtils.greenStyle());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void send(MsgClient msg) {
		try {
			Out.writeObject(msg);
			Out.flush();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	void start() throws Exception {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				MsgClient msg = (MsgClient) In.readObject();
				ui.areaMsg.insertString(ui.areaMsg.getLength(), "\n"+msg.getSender() + " > Me] : " + msg.getContent(), FontUtils.greenStyle());
			} catch (Exception e) {
			}
		}
	}
}

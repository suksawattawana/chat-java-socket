package com.chat.client.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

import com.chat.client.cores.model.MsgClient;
import com.chat.client.cores.services.ClientManager;

public class ApplicationClient {
	static String username, password;
	public static void main(String[] args) {
		ClientManager manger = new ClientManager();
		
		// connect server
		try {
			String arr = InetAddress.getLocalHost().getHostAddress();
			int port = 13000;
			
			String msg = manger.startAppClient(arr, port);
			System.out.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// send massage
		try (Scanner sc = new Scanner(System.in)) {
			String target = "All";
			username = "SARM";
			String msg = "";
			while(msg != "exit") {
				System.out.print("Msg: ");
			    msg = sc.nextLine();
			    if(!msg.isEmpty()){
			    	manger.send(new MsgClient("message", username, msg, target));
			    }
			}
		}
		
	}

}

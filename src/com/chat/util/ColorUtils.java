package com.chat.util;

import java.awt.Color;

public class ColorUtils {
	
	public static Color green() {
		return new Color(0, 205, 50);
	}
	
	public static Color red() {
		return new Color(255, 0, 0);
	}
	
	public static Color mainColor() {
		return new Color(179, 230, 255);
	}
}

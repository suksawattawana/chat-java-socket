package com.chat.util;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JScrollPane;

import com.chat.util.component.ButtonStyle;
import com.chat.util.component.TableStyle;
import com.chat.util.component.TextAreaStyle;
import com.chat.util.component.TextFieldStyle;
import com.chat.util.component.TextPaneStyle;

public class ComponentUtils {

	private static TextFieldStyle jTextField;
	private static ButtonStyle btnButton;
	private static TextAreaStyle textArea;
	private static TextPaneStyle textPane;
	private static TableStyle table;
	private static JScrollPane jScrollPane;
    
	public static TextFieldStyle fieldInput(int size) {
		jTextField = new TextFieldStyle();
		jTextField.setColumns(size);
		return jTextField;
	}
	
	public static TextFieldStyle fieldInput(Boolean enabled) {
		jTextField = new TextFieldStyle();
		jTextField.setEnabled(enabled);
		return jTextField;
	}
	
	public static TextFieldStyle fieldInput() {
		jTextField = new TextFieldStyle();
		btnButton.setFont(new Font("Cordia New", Font.PLAIN, 16));
		return jTextField;
	}
	
	public static ButtonStyle btnButton(String text, int size, Boolean enabled) {
		btnButton = new ButtonStyle();
		btnButton.setText(text);
		btnButton.setFont(new Font("Cordia New", Font.BOLD, 16));
		btnButton.setEnabled(enabled);
		return btnButton;
	}
	
	public static TextAreaStyle textArea() {
		textArea = new TextAreaStyle();
		return textArea;
	}
	
	public static TextAreaStyle textArea(Boolean editable) {
		textArea = new TextAreaStyle();
		textArea.setEditable(editable);
		return textArea;
	}
	
	public static TextPaneStyle textPane() {
		textPane = new TextPaneStyle();
		return textPane;
	}
	
	public static TextPaneStyle textPane(Boolean editable) {
		textPane = new TextPaneStyle();
		textPane.setEditable(editable);
		return textPane;
	}
	
	public static TableStyle table() {
		table = new TableStyle();
		return table;
	}
	
	public static JScrollPane jScrollPanel(Component view) {
		jScrollPane = new JScrollPane();
		jScrollPane.setViewportView(view);
		return jScrollPane;
	}
	
}

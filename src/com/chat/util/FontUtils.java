package com.chat.util;

import java.awt.Color;
import java.awt.Font;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import com.chat.util.component.LabelStyle;
import com.chat.util.global.FontStyle;

public class FontUtils {
	
	private static LabelStyle jLabel;
	
	/*
	 * 0 - Regular
	 * 1 - Bold
	 * 2 - Italic
	 * 3 - Italic Bold
	 */
	public static LabelStyle font16B(String massage) {
		jLabel = new LabelStyle();
		jLabel.setFont(FontStyle.cordiaNew(Font.BOLD, 16));
		jLabel.setText(massage);
		return jLabel;
	}
	
	public static LabelStyle font16(String massage) {
		jLabel = new LabelStyle();
		jLabel.setText(massage);
		return jLabel;
	}
	
	public static LabelStyle font16(String massage, Color color) {
		jLabel = new LabelStyle();
		jLabel.setText(massage);
		jLabel.setForeground(color);
		return jLabel;
	}
	
	public static SimpleAttributeSet greenStyle() {
		SimpleAttributeSet style = new SimpleAttributeSet();
		StyleConstants.setForeground(style, ColorUtils.green());
		return style;
	}
	
	public static SimpleAttributeSet blueStyle() {
		SimpleAttributeSet style = new SimpleAttributeSet();
		StyleConstants.setForeground(style, Color.BLUE);
		return style;
	}
	
}

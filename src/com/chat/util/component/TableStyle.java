package com.chat.util.component;

import javax.swing.JTable;

import com.chat.util.global.FontStyle;

public class TableStyle extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TableStyle() {
		super.setFont(FontStyle.cordiaNew());
	}

}

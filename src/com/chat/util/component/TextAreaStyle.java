package com.chat.util.component;

import javax.swing.JTextArea;

import com.chat.util.global.FontStyle;

public class TextAreaStyle extends JTextArea {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TextAreaStyle() {
		super.setFont(FontStyle.cordiaNew());
	}
}

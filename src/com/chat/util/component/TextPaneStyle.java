package com.chat.util.component;

import javax.swing.JTextPane;

import com.chat.util.global.FontStyle;

public class TextPaneStyle extends JTextPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TextPaneStyle() {
		super.setFont(FontStyle.cordiaNew());
	}
}

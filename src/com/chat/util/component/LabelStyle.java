package com.chat.util.component;

import javax.swing.JLabel;

import com.chat.util.global.FontStyle;

public class LabelStyle extends JLabel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LabelStyle() {
		super.setFont(FontStyle.cordiaNew());
	}

}

package com.chat.util.component;

import javax.swing.JTextField;

import com.chat.util.global.FontStyle;

public class TextFieldStyle extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TextFieldStyle() {
		super.setFont(FontStyle.cordiaNew());
	}
	
}

package com.chat.util.global;

import java.awt.Font;

public class FontStyle {
	
	private final static String CORDIA_NEW = "Cordia New";
	
	public static Font cordiaNew() {
		return new Font(CORDIA_NEW, Font.PLAIN, 16);
	}
	
	public static Font cordiaNew(int size) {
		return new Font(CORDIA_NEW, Font.PLAIN, size);
	}
	
	public static Font cordiaNew(int tyle, int size) {
		return new Font(CORDIA_NEW, tyle, size);
	}
}

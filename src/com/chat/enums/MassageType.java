package com.chat.enums;

public enum MassageType {
	MASSAGE("massage"), LOGIN("login");

	private String value;

	private MassageType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
